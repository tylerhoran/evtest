Rails.application.routes.draw do
  resources :events
  resources :attendees, only: [:destroy]
  devise_for :users
  authenticated :user do
    root to: 'events#index', as: :authenticated_root
  end
  unauthenticated do
    root to: 'visitors#index', as: :unauthenticated_root
  end
end
