# EvTest #

## Introduction ##
This site consists of the following core elements:

1. Rails 4.2.1
2. Ruby 2.2.2
3. Heroku Deployment at http://evtest.herokuapp.com
4. Postgresql Database


## Development Environment ##
To set up the development environment on OSX:

1. Install RVM and bundle install

    ```
    \curl -sSL https://get.rvm.io | bash
    ```

2. Install postgresql

    ```
    brew install postgresql
    ```

    Adding the script to launch postres on login

    ```
    mkdir -p ~/Library/LaunchAgents
    ```

    ```
    cp /usr/local/Cellar/postgresql/9.3.2/homebrew.mxcl.postgresql.plist ~/Library/LaunchAgents/
    ```

    ```
    launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
    ```

3. Run the rake database create and seed scripts

    ```
    rake db:create
    rake db:migrate
    rake db:seed
    ```

### Development Elements ###

-   Template Engine: Haml
-   Testing Framework: RSpec and Factory Girl
-   Front-end Framework: Bootstrap 3 (Sass)
-   Form Builder: SimpleForm
-   Authentication: Devise
-   Server: Pow

## Testing Environment ##

The test suite consists of the following elements:

1. Rspec
2. Capybara
3. Cucumber
4. Factory Girl
5. Guard
6. Spring

To run the full test suite from project root:

  ```
  guard ↩
  ```
## Deploying the application ##

To deploy to all applications:

  ```
  git push heroku master
  ```


