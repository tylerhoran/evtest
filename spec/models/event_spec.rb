require 'rails_helper'

describe Event, type: :model do
  let(:event) { FactoryGirl.build(:event) }

  it 'should be valid with all fields' do
    expect(event).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      event.user = nil
      expect(event).to_not be_valid
    end

    it 'a title' do
      event.title = nil
      expect(event).to_not be_valid
    end
  end
end
