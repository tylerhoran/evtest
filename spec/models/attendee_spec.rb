require 'rails_helper'

describe Attendee, type: :model do
  let(:attendee) { FactoryGirl.build(:attendee) }

  it 'should be valid with all fields' do
    expect(attendee).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      attendee.name = nil
      expect(attendee).to_not be_valid
    end

    it 'an email' do
      attendee.email = nil
      expect(attendee).to_not be_valid
    end
  end
end
