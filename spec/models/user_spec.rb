require 'rails_helper'

describe User, type: :model do
  let(:user) { FactoryGirl.build(:user) }

  it 'should be valid with all fields' do
    expect(user).to be_valid
  end

  describe 'should require' do
    it 'an email' do
      user.email = nil
      expect(user).to_not be_valid
    end
  end
end
