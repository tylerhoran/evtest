step 'I visit the home page' do
  visit '/'
end

step 'I sign in' do
  user = FactoryGirl.create(:user)
  visit new_user_session_path
  fill_in 'user_email', with: user.email
  fill_in 'user_password', with: user.password
  click_button 'Sign in'
  expect(page).to have_content 'Events'
end

step 'I create an event' do
  title = 'Howdy'
  visit new_event_path
  fill_in 'Title', with: title
  click_button 'Create Event'
end

step 'it should appear on my event list' do
  visit authenticated_root_path
  expect(page).to have_content 'Howdy'
end

step 'I am not logged in' do
  visit unauthenticated_root_path
end

step 'I visit the events page' do
  visit events_path
end

step 'I should be directed to log in' do
  expect(page).to have_content 'You need to sign in or sign up before continuing'
end

step "I visit the event path of another user's event" do
  event = FactoryGirl.create(:event)
end

step 'I should receive an error' do
  visit event_path(Event.last)
  expect(page).to have_content ActiveRecord::RecordNotFound
end

step 'I have created an event' do
  user = FactoryGirl.create(:user)
  visit new_user_session_path
  fill_in 'Email', with: user.email
  fill_in 'Password', with: user.password
  click_button 'Sign in'
  FactoryGirl.create(:event, user: user, title: 'My Event Title')
end

step 'I visit the event and edit it' do
  visit edit_event_path(Event.last)
  fill_in 'Title', with: 'New Title'
  click_button 'Update Event'
end

step 'it should be updated' do
  expect(page).to have_content 'Event was successfully updated.'
end

step 'I visit the events page' do
  visit events_path
end

step 'I can delete it' do
  click_link 'Destroy'
  expect(page).to_not have_content 'My Event Title'
  expect(page).to have_content 'Event was successfully destroyed.'
end

step 'I am on the event page of an event with attendees' do
  event = FactoryGirl.create(:event, user: User.last, title: 'My Event Title')
  FactoryGirl.create_list(:attendee, 4, event: event)
  visit event_path(event)
end

step 'I click remove attendee' do
  all(:link, 'Remove attendee').first.click
end

step 'the attendee should not be on the event page' do
  expect(page).to have_content 'Attendee was removed.'
  expect(all(:link, 'Remove attendee').count).to eq(3)
end

step 'I create an event without all fields' do
  visit new_event_path
  click_button 'Create Event'
end

step 'it should redirect be back with an error' do
  expect(page).to have_content "Title can't be blank"
end

step 'I update the event with missing fields' do
  visit edit_event_path(Event.last)
  fill_in 'Title', with: ''
  click_button 'Update Event'
end
