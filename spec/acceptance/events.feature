Feature: a user's events
  Scenario: can be created when logged in and seen on the event list
    Given I visit the home page
    And I sign in
    When I create an event
    Then it should appear on my event list
  Scenario: cannot be created without all required fields
    Given I visit the home page
    And I sign in
    When I create an event without all fields
    Then it should redirect be back with an error
  Scenario: cannot be updated without all required fields
    Given I have created an event
    When I update the event with missing fields
    Then it should redirect be back with an error
  Scenario: cannot be created when not authenticated
    Given I am not logged in 
    When I visit the events page
    Then I should be directed to log in
  Scenario: cannot be seen by other users 
    Given I sign in
    When I visit the event path of another user's event
    Then I should receive an error
  Scenario: can be edited once created
    Given I have created an event
    When I visit the event and edit it
    Then it should be updated
  Scenario: can be deleted
    Given I have created an event
    When I visit the events page
    Then I can delete it
  Scenario: removing attendees
    Given I sign in
    And I am on the event page of an event with attendees
    When I click remove attendee
    Then the attendee should not be on the event page