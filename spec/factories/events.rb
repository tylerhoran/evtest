FactoryGirl.define do
  factory :event do
    title { Faker::Company.catch_phrase }
    association :user
  end
end
