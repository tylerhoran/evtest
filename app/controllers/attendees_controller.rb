# Controller for attendees ( mostly to destroy )
class AttendeesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_attendee

  def destroy
    @attendee.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Attendee was removed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_attendee
    @attendee = Attendee.find(params[:id])
  end
end
