class Event < ActiveRecord::Base
  validates :title, presence: true
  validates :user, presence: true
  has_many :attendees
  belongs_to :user

  accepts_nested_attributes_for :attendees, reject_if: :all_blank, allow_destroy: true
end
